package come.codetypo.iolab.Services;

import come.codetypo.iolab.Entities.Product;
import come.codetypo.iolab.Repositories.ProductRepo;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepo productRepo;

    public ProductService(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public Optional<Product> find (Long id){
        return productRepo.findById(id);
    }

    public Iterable<Product>findALl(){
        return productRepo.findAll();
    }

    public Product save(Product product){
        return productRepo.save(product);
    }

    public void deleteById(Long id){
        productRepo.deleteById(id);
    }

    public Product updateProduct(Product product, long id) {
        Optional<Product> dbProduct = productRepo.findById(id);
        dbProduct.get().setName(product.getName());
        dbProduct.get().setPrice(product.getPrice());
        dbProduct.get().setAvailable(product.isAvailable());
        return productRepo.save(dbProduct.get());
    }

    public Product patch(Map<String, Object> patchNotes, long id) {
        Optional<Product> dbProduct = productRepo.findById(id);
        if (dbProduct.isPresent()) {
            Product p = dbProduct.get();
            if (patchNotes.containsKey("name")) p.setName((String) patchNotes.get("name"));
            if (patchNotes.containsKey("available")) p.setAvailable((Boolean) patchNotes.get("available"));
            if (patchNotes.containsKey("price")) p.setPrice((Double) patchNotes.get("price"));
            productRepo.save(p);
        }
        return null;
    }

}