package come.codetypo.iolab.Services;

import come.codetypo.iolab.DTO.UserDTO;
import come.codetypo.iolab.Entities.User;
import come.codetypo.iolab.Repositories.UserRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
public class UserService {

    private UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public UserDTO validate(UserDTO user){
        List<User> users = (List<User>) userRepo.findAll();
        for(User u : users ) {
            if (u.getName().equals(user.getName()) && u.getPassword().equals(user.getPassword()))
                return new UserDTO(u.getName(),u.getPassword(),u.getRole());
        }
        return null;
    }

}
