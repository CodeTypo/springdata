package come.codetypo.iolab.Services;

import come.codetypo.iolab.Entities.Customer;
import come.codetypo.iolab.Entities.Order;
import come.codetypo.iolab.Entities.Product;
import come.codetypo.iolab.Repositories.CustomerRepo;
import come.codetypo.iolab.Repositories.OrderRepo;
import come.codetypo.iolab.Repositories.ProductRepo;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrderService {

    private final OrderRepo orderRepo;
    private final CustomerRepo customerRepo;
    private final ProductRepo productRepo;

    public OrderService(OrderRepo orderRepo, CustomerRepo customerRepo, ProductRepo productRepo) {
        this.orderRepo = orderRepo;
        this.customerRepo = customerRepo;
        this.productRepo = productRepo;
    }

    public Optional<Order> find (Long id){
        return orderRepo.findById(id);
    }

    public Iterable<Order>findALl(){
        return orderRepo.findAll();
    }

    public Order save(Order order){
        return orderRepo.save(order);
    }

    public Order updateOrder(Order order, long id) {
        if(this.find(id).isEmpty()){
            return null;
        }
        Optional<Order> dbOrder = this.find(id);
        if(customerRepo.findById(order.getCustomer().getId()).isPresent()){
            dbOrder.get().setCustomer(customerRepo.findById(order.getCustomer().getId()).get());
        }
        Set<Product> productsToUpdate = order.getProducts();
        Set<Product> verifiedProducts = new HashSet<>();
        for (Product product : productsToUpdate){
            if (productRepo.findById(product.getId()).isPresent()){
                Product existing = productRepo.findById(product.getId()).get();
                verifiedProducts.add(existing);
            }
        }
        dbOrder.get().setProducts(verifiedProducts);
        dbOrder.get().setStatus(order.getStatus());
        return orderRepo.save(dbOrder.get());
    }

    public Order patch(Map<String, Object> patchNotes, long id) {
        Optional<Order> dbOrder = orderRepo.findById(id);
        if (dbOrder.isPresent()) {
            Order o = dbOrder.get();
            if (patchNotes.containsKey("status")) {
                o.setStatus((String) patchNotes.get("status"));
            }
            if (patchNotes.containsKey("customerID")) {
                Long newCustomerId = Long.valueOf((String) patchNotes.get("customerID"));
                if (customerRepo.findById(newCustomerId).isPresent()) {
                    o.setCustomer(customerRepo.findById(newCustomerId).get());
                }
                if (patchNotes.containsKey("productIDs")) {
                    ArrayList <Integer> productIDs = (ArrayList<Integer>) patchNotes.get("productIDs");
                    Set<Product> newProducts = new HashSet<>();
                    for (int productId : productIDs) {
                        if (productRepo.findById((long) productId).isPresent()) {
                            newProducts.add(productRepo.findById((long) productId).get());
                        }
                    }
                    o.setProducts(newProducts);
                }
                orderRepo.save(o);
            }
            return o;
        }
        return null;
    }
}
