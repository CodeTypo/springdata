package come.codetypo.iolab.Services;
import come.codetypo.iolab.Entities.Customer;
import come.codetypo.iolab.Repositories.CustomerRepo;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class CustomerService {

    private final CustomerRepo customerRepo;

    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public Optional<Customer> find(Long id) {
        return customerRepo.findById(id);
    }

    public Iterable<Customer> findALl() {
        return customerRepo.findAll();
    }

    public Customer save(Customer customer) {
        return customerRepo.save(customer);
    }

    public Customer updateCustomer(Customer customer, long id) {
        Optional<Customer> dbCustomer = customerRepo.findById(id);
        if(dbCustomer.isPresent()) {
            dbCustomer.get().setName(customer.getName());
            dbCustomer.get().setAddress(customer.getAddress());
            return customerRepo.save(dbCustomer.get());
        }
        else return null;
    }

    public Customer patch(Map<String, Object> patchNotes, long id) {
        Optional<Customer> dbCustomer = customerRepo.findById(id);
        if (dbCustomer.isPresent()) {
            Customer c = dbCustomer.get();
            if (patchNotes.containsKey("name")) c.setName((String) patchNotes.get("name"));
            if (patchNotes.containsKey("address")) c.setAddress((String) patchNotes.get("address"));
            customerRepo.save(c);
            return  c;
        }
        return null;
    }
}
