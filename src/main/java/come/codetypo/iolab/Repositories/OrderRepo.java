package come.codetypo.iolab.Repositories;

import come.codetypo.iolab.Entities.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepo extends CrudRepository <Order, Long> {
}
