package come.codetypo.iolab.Controllers;

import come.codetypo.iolab.Entities.Product;
import come.codetypo.iolab.Services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {

    private final ProductService products;

    @Autowired
    public ProductController(ProductService products){
        this.products = products;
    }

    @GetMapping("/product/all")
    public Iterable<Product> getAll(){
        return products.findALl();
    }

    @GetMapping("/product")
    public Optional<Product> getById (Long id){
        return products.find(id);
    }

    @PostMapping("/admin/product")
    public Product addProduct (@RequestBody Product product){
        return products.save(product);
    }

    @PatchMapping("/admin/product/{id}")
    public Product patchProduct(@RequestBody Map<String, Object> patchNotes, @PathVariable long id){
        return (Product) products.patch(patchNotes,id);
    }

    @PutMapping("/admin/product/{id}")
    public Product updateProduct(@RequestBody Product product, @PathVariable long id) {
        if(products.find(id).isPresent()) {
            return products.updateProduct(product, id);
        }
        return null;
    }
}
