package come.codetypo.iolab.Controllers;

import come.codetypo.iolab.DTO.UserDTO;
import come.codetypo.iolab.Entities.User;
import come.codetypo.iolab.Services.OrderService;
import come.codetypo.iolab.Services.UserService;
import come.codetypo.iolab.Util.UserDtoBuilder;
import come.codetypo.iolab.Util.WebSecurityConfig;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@RestController
public class LoginController {

    private UserService userService;

    @Autowired
    public LoginController(UserService userService){
        this.userService = userService;
    }

    @Autowired
    private final PasswordEncoder passwordEncoder = new WebSecurityConfig().getPasswordEncoder();

    @PostMapping("/getToken")
    public String login(@RequestBody User user) {
        long time = System.currentTimeMillis();

        UserDTO validatedUser = userService.validate(new UserDtoBuilder().buildUserDTO(user));
        if(validatedUser != null){
         return Jwts.builder()
                .setSubject(validatedUser.getName())
                .claim("role",validatedUser.getRole())
                .setIssuedAt(new Date(time))
                .setExpiration(new Date(time + 60000))
                .signWith(SignatureAlgorithm.HS256,"$~xFGA)Od<#9~~S|zAYYIra8FUJrRy".getBytes(StandardCharsets.UTF_8))
                .compact();
        } else return "Validation error";
    }
}
