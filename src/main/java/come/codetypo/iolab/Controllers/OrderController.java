package come.codetypo.iolab.Controllers;

import come.codetypo.iolab.Entities.Order;
import come.codetypo.iolab.Services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderController {

    private final OrderService orders;

    @Autowired
    public OrderController(OrderService orders){
        this.orders = orders;
    }

    @GetMapping("/order/all")
    public Iterable<Order> getAll(){
        return orders.findALl();
    }

    @GetMapping("/order")
    public Optional<Order> getById (Long id){
        return orders.find(id);
    }

    @PostMapping("/order")
    public Order addOrder (@RequestBody Order order){
        return orders.save(order);
    }

    @PatchMapping("/admin/order/{id}")
    public Order patchOrder(@RequestBody Map<String, Object> patchNotes, @PathVariable long id){
        return orders.patch(patchNotes,id);
    }

    @PutMapping("/admin/order/{id}")
    public Order updateOrder(@RequestBody Order order, @PathVariable long id) {
        if(orders.find(id).isPresent()) {
            return orders.updateOrder(order, id);
        }
        return null;
    }


}
