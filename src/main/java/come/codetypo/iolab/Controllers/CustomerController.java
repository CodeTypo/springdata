package come.codetypo.iolab.Controllers;

import come.codetypo.iolab.Entities.Customer;
import come.codetypo.iolab.Services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerController {

    private final CustomerService customers;

    @Autowired
    public CustomerController(CustomerService customers){
        this.customers = customers;
    }

    @GetMapping("/customer/all")
    public Iterable<Customer> getAll(){
        return customers.findALl();
    }

    @GetMapping("/customer")
    public Optional<Customer> getById (Long id){
        return customers.find(id);
    }

    @PostMapping("/admin/customer")
    public Customer addCustomer (@RequestBody Customer customer){
        return customers.save(customer);
    }

    @PatchMapping("/admin/customer/{id}")
    public Customer patchCustomer(@RequestBody Map<String, Object> patchNotes, @PathVariable long id){
         return customers.patch(patchNotes,id);
    }

    @PutMapping("/admin/customer/{id}")
    public Customer updateCustomer(@RequestBody Customer customer, @PathVariable long id) {
        if(customers.find(id).isPresent()){
            return customers.updateCustomer(customer, id);
        }
        return null;
    }
}
