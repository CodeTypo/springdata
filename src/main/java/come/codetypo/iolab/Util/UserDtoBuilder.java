package come.codetypo.iolab.Util;

import come.codetypo.iolab.DTO.UserDTO;
import come.codetypo.iolab.Entities.User;

public class UserDtoBuilder {

    public UserDTO buildUserDTO (User user){
        return new UserDTO(user.getName(),user.getPassword());
    }

    public User buildUser (UserDTO dto){
        return new User (dto.getName(), dto.getPassword(),dto.getRole());
    }

}
