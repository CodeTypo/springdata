package come.codetypo.iolab.Util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Set;

public class JwtFilter extends BasicAuthenticationFilter {

    public JwtFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);

    }

    private final PasswordEncoder passwordEncoder = new WebSecurityConfig().getPasswordEncoder();



    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            String header = request.getHeader("Authorization");
            UsernamePasswordAuthenticationToken authResult = getAuthenticationByToken(header);
            SecurityContextHolder.getContext().setAuthentication(authResult);
            chain.doFilter(request, response);
        } catch (Exception e){
            ((HttpServletResponse) response).setHeader("Content-Type", "application/json");
            ((HttpServletResponse) response).setStatus(401);
            response.getOutputStream().write(("Authorization error: The token You provided is either expired or invalid or Your role privilleges are too low").getBytes(StandardCharsets.UTF_8));
            return;
        }
    }

    private UsernamePasswordAuthenticationToken getAuthenticationByToken(String header) {
        //System.out.println("header");
        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey("$~xFGA)Od<#9~~S|zAYYIra8FUJrRy".getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(header.replace("Bearer",""));
        //System.out.println(claimsJws.toString());

        String username = claimsJws.getBody().get("sub").toString();
        String role = claimsJws.getBody().get("role").toString();

        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = Collections.singleton(new SimpleGrantedAuthority(role));

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                = new UsernamePasswordAuthenticationToken(username,null,simpleGrantedAuthorities);

        return usernamePasswordAuthenticationToken;
    }
}
