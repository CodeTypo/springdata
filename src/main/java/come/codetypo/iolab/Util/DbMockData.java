package come.codetypo.iolab.Util;

import come.codetypo.iolab.Entities.Customer;
import come.codetypo.iolab.Entities.Order;
import come.codetypo.iolab.Entities.Product;
import come.codetypo.iolab.Entities.User;
import come.codetypo.iolab.Repositories.CustomerRepo;
import come.codetypo.iolab.Repositories.OrderRepo;
import come.codetypo.iolab.Repositories.ProductRepo;
import come.codetypo.iolab.Repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepo orderRepository;
    private CustomerRepo customerRepository;
    private UserRepo userRepository;

    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepo orderRepository, CustomerRepo customerRepository, UserRepo userRepo) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userRepository = userRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Korek", 2f, true);
        Product product1 = new Product("Rura", 5f, true);
        Customer customer = new Customer("Jak Kowalski", "Wrocław");
        Customer customer1 = new Customer("Antek Stasiak", "Legnica");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }};
        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");

        User basicUser = new User("zwyklyUser","123qwe","ROLE_USER");
        User adminUser = new User("adminUser","123qwe","ROLE_ADMIN");

        productRepository.save(product);
        productRepository.save(product1);
        customerRepository.save(customer);
        customerRepository.save(customer1);
        orderRepository.save(order);
        userRepository.save(basicUser);
        userRepository.save(adminUser);
    }
}